require 'rails_helper'

RSpec.describe 'product.decorator', type: :model do
  let(:taxon) { create(:taxon) }
  let(:taxon_1) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], name: 'bag') }
  let!(:product_2) { create(:product, taxons: [taxon], name: 'shirt') }
  let!(:product_3) { create(:product, taxons: [taxon_1], name: 'toto') }

  describe 'related_product メソッド' do
    it '同じカテゴリーの商品だけを返す' do
      expect(product.related_products).to match_array([product_2])
      expect(product.related_products).not_to match_array([product_3])
    end
  end
end
