require 'rails_helper'

RSpec.describe 'Helperes', type: :feature do
  describe 'helper method' do
    include ApplicationHelper

    describe 'full_titleメソッド' do
      subject { full_title(title) }

      context "引数が省略された場合" do
        it { is_expected.to eq APP_TITLE }
      end

      context "nilが渡された場合" do
        let(:title) { nil }

        it { is_expected.to eq APP_TITLE }
      end

      context "空文字が渡された場合" do
        let(:title) { '' }

        it { is_expected.to eq APP_TITLE }
      end

      context "'hello'が渡された場合" do
        let(:title) { 'hello' }

        it { is_expected.to eq "hello - #{APP_TITLE}" }
      end
    end
  end
end
