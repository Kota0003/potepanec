require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:taxon) { create(:taxon) }
  let!(:product_1) { create(:product, taxons: [taxon]) }
  let!(:taxonomy) { taxon.taxonomy }

  before do
    get :show, params: { id: taxon.id }
  end

  describe 'category#show' do
    it 'showにレンダリングされているか' do
      expect(response).to render_template :show
      expect(response.status).to eq(200)
    end

    it 'インスタンス変数が正しく設定されているか' do
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:products)).to match_array product_1
      expect(assigns(:taxonomies)).to match_array taxonomy
    end
  end
end
