require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], name: 'bag') }
  let!(:related_product) { create(:product, taxons: [taxon], name: 'shirt') }

  before do
    get :show, params: { id: product.id }
  end

  describe 'product#show' do
    it 'showがレンダリングされているか' do
      expect(response).to render_template :show
      expect(response.status).to eq(200)
    end
  end

  it '@productが正しく設定されているか' do
    expect(assigns(:product)).to eq product
  end

  it '@related_products 関連商品のproduct.nameが一致しているか' do
    expect(assigns(:related_products)).to match_array([related_product])
  end
end
