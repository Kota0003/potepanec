require 'rails_helper'

RSpec.describe 'categories', type: :feature do
  describe 'show page' do
    include ApplicationHelper
    let!(:taxonomy) { create(:taxonomy, name: 'Category') }
    let(:taxon_1) { create(:taxon, name: 'bag') }
    let(:taxon_2) { create(:taxon, name: 'mag') }
    let!(:product_1) { create(:product, name: 'bag1', taxons: [taxon_1]) }
    let!(:product_2) { create(:product, name: 'mag1', taxons: [taxon_2]) }

    before do
      visit potepan_category_path(taxon_1.id)
    end

    it 'productが表示されているか' do
      expect(page).to have_content taxonomy.name
      within '.productCaption' do
        expect(page).to have_content product_1.name
        expect(page).to have_content product_1.display_price
      end
    end

    it 'magのカテゴリーページへ移動しbagが表示されていないか' do
      visit potepan_category_path(Spree::Taxon.find_by(name: 'mag').id)
      expect(page).to have_title full_title(taxon_2.name)
      expect(page).to have_content 'mag1'
      expect(page).not_to have_content taxon_1.name
    end

    it 'product_1の詳細ページへ移動し、"一覧ページへ戻る"ボタンを押し最初のページへ戻ってこれるか' do
      click_on product_1.name
      expect(current_path).to eq potepan_product_path(product_1.id)
      expect(page).to have_title full_title(product_1.name)
      expect(page).to have_content product_1.display_price
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(product_1.taxon_ids.min)
      expect(page).to have_title full_title(taxon_1.name)
    end
  end
end
