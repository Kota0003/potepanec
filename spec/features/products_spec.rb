require 'rails_helper'

RSpec.describe 'Products', type: :feature do
  describe 'product show' do
    include ApplicationHelper
    subject { page }

    let(:taxon) { create(:taxon) }
    let(:taxon_1) { create(:taxon) }
    let!(:product_4) { create(:product, taxons: [taxon_1]) }

    CREATE_TEST_PRODUCTS_COUNT.times do |n|
      let!(:"same_taxon_product_#{n}") { create(:product, taxons: [taxon]) }
    end

    before do
      visit potepan_product_path(same_taxon_product_0.id)
    end

    it 'titleがproduct.nameになっているか' do
      is_expected.to have_title full_title(same_taxon_product_0.name)
    end

    it 'move to cart page' do
      is_expected.to have_content 'カートへ入れる'
      click_on 'カートへ入れる'
      expect(current_path).to eq potepan_cart_page_path
    end

    context '関連商品が5つ以上存在するとき' do
      let!(:product_5) { create(:product, taxons: [taxon]) }

      it '商品の詳細が表示され、同じカテゴリーの関連商品が４つ表示されているか' do
        visit potepan_product_path(same_taxon_product_0.id)
        within '.media-body' do
          is_expected.to have_content same_taxon_product_0.display_price
          is_expected.to have_content same_taxon_product_0.description
        end
        within '.productsContent' do
          is_expected.to have_content same_taxon_product_1.name
          is_expected.to have_content same_taxon_product_2.name
          is_expected.to have_content same_taxon_product_3.name
          is_expected.not_to have_content product_4.name
          is_expected.to have_content product_5.name
        end
      end
    end
  end
end
