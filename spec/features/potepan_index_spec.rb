require 'rails_helper'

RSpec.describe 'index', type: :feature do
  describe 'start page' do
    before do
      visit potepan_path
    end

    it 'have a title' do
      expect(page).to have_title APP_TITLE
    end

    it '表示確認' do
      expect(page).to have_content '人気カテゴリー'
      expect(page).to have_content '新着商品'
    end
  end
end
